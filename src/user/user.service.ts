import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateUserInput } from './dto/create-user.input';
import { User } from './user.entity';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User)
    private userRepository: Repository<User>,
  ) {}

  async findAllUsers(): Promise<User[]> {
    const users = await this.userRepository.find();
    return users;
  }

  async createUser(data: CreateUserInput): Promise<User> {
    try{
    const user = await this.userRepository.create(data);
    const userSaved = await this.userRepository.create(user);
      
    if (!userSaved) {
      throw new InternalServerErrorException('Problema para criar um usuário.');
    }
    return userSaved;
  }catch(error){
    throw new InternalServerErrorException('Probleminha')
  }
  }
}
